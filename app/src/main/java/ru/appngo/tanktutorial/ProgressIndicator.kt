package ru.appngo.tanktutorial


interface ProgressIndicator {

    fun showProgress()

    fun dismissProgress()
}