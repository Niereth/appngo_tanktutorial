package ru.appngo.tanktutorial.models

import android.view.View
import ru.appngo.tanktutorial.enums.Direction


data class Bullet(
    val view: View,
    val direction: Direction,
    val tank: Tank,
    var canMoveFurther: Boolean = true
)