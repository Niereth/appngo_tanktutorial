package ru.appngo.tanktutorial.models

data class Coordinate(
    val top: Int,
    val left: Int
)