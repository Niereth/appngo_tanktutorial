package ru.appngo.tanktutorial.models

import android.view.View
import ru.appngo.tanktutorial.enums.Material


data class Element(
    val viewId: Int = View.generateViewId(),
    val material: Material,
    var coordinate: Coordinate
)