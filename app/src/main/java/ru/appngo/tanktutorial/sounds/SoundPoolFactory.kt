package ru.appngo.tanktutorial.sounds

import android.media.SoundPool

class SoundPoolFactory {

    private val maxStreamsAmount = 6

    fun createSoundPool(): SoundPool {
        return SoundPool.Builder()
            .setMaxStreams(maxStreamsAmount)
            .build()
    }
}